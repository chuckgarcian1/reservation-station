/*
    Programmed By: Charles "Chuck" Garcia
    Email: Chuckgarcian@gmail.com

    Description: Code file for testing dispatch-queue
*/

module dq_tb ();
  initial begin
    $display ("Hello world! \n");
  end
endmodule