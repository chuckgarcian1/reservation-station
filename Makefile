all: 
	iverilog -g2012 dq-tb.sv dispatch-queue.sv -o test.out

run: all
	vvp test.out

clean:
	rm test.out
