/*
  Description: RTL definition of dispatch queue 
*/

module dispatch_queue #(parameter data_width )(
  input wire clk,
  input wire rst,
  input wire enqueue,
  input wire dequeue,
  
  output wire [data_width] read_instr,
  output wire full
);
  integer head_idx;
  integer tail_idx;

  
endmodule
